﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

[Serializable]
public class TotalData
{
    public int confirmed;
    public int deaths;
    public int recovered;
    public string dt;
    public double ts;
}