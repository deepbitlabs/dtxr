﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour
{

    LocationInfo currentLocation;

    void Awake()
    {
        Debug.Log(" Location Name: " + currentLocation);
    }


    IEnumerator Start()
    {
        if (!Input.location.isEnabledByUser)
            yield break;

        Input.location.Start();

        int maxWait = 20;
        while(Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);

            maxWait--;
        }

        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            //InvokeRepeating("ReceiveLocationData", 0, 3);

            print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + "" + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
        }

        Input.location.Stop();
    }

    void ReceiveLocationData()
    {
        currentLocation = Input.location.lastData;
        print(" Location Name: " + currentLocation);
    }

}
