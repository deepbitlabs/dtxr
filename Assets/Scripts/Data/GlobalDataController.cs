﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

[Serializable]
public class GlobalDataController
{
    public int updated;

    public int cases;

    public int todayCases;

    public int deaths;

    public int todayDeaths;

    public int recovered;

    public int active;

    public int critical;

    public int casesPerOneMillion;

    public int deathsPerOneMillion;

    public int tests;

    public double testsPerOneMillion;

    public int affectedCountries;
}
