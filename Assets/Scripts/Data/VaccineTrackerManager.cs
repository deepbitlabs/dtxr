﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class VaccineTrackerManager : MonoBehaviour
{

    [Header(" Source header ")]
    public Text source_header;


    void Start()
    {
        StartCoroutine(getVaccineData());
    }




    IEnumerator getVaccineData()
    {
        UnityWebRequest www = UnityWebRequest.Get("https://disease.sh/v3/covid-19/vaccine");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
            Debug.LogWarning(jsonResult);

            VaccineTrackerController vaccineController = JsonUtility.FromJson<VaccineTrackerController>(jsonResult);


            foreach (PhasesItem phases in vaccineController.phases)
            {
                Debug.Log(" Current Phase " + phases.phase);
                Debug.Log(" Current Candidates " + phases.candidates);
            }
        }
    }
}
