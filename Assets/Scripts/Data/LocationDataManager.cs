﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine.UI;

public class LocationDataManager : MonoBehaviour
{
    public Text confirmed_value;
    public Text recovered_value;
    public Text tested_value;

    void Start()
    {
        StartCoroutine(getData());
    }

    IEnumerator getData()
    {
        UnityWebRequest www = UnityWebRequest.Get("https://covid2019-api.herokuapp.com/v2/current/US");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {


            byte[] results = www.downloadHandler.data;

            if (www.isDone)
            {
                string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                Debug.Log(jsonResult);


                LocationDataController locationDataController = JsonUtility.FromJson<LocationDataController>(jsonResult);

                foreach (DataItem data in locationDataController.data)
                {
                    //Debug.Log(data.Province_State);
                    //Debug.Log(data.Confirmed);
                    //Debug.Log(data.Recovered);
                    //Debug.Log(data.Active);
                    //Debug.Log(data.People_Tested);

                    if (data.Province_State == "New York")
                    {
                        confirmed_value.text = data.Confirmed.ToString("0,0,0,0");
                        
                        recovered_value.text = data.Recovered.ToString("0,0,0,0");
                        tested_value.text = data.People_Tested.ToString("0,0,0,0");

                    }
                }

                //Debug.Log(locationDataController.Country);
                //Debug.Log(locationDataController.Province);
                //Debug.Log(locationDataController.Lat);
                //Debug.Log(locationDataController.Lon);

                //Debug.Log(globalDataController.totalConfirmed);
                //Debug.Log(globalDataController.totalRecovered);

                //confirmed_value.text = globalDataController.totalConfirmed.ToString();
                //recovered_value.text = globalDataController.totalRecovered.ToString();
                //Debug.Log(" Here are the confirmed cases " + totalData.confirmed);
                //Debug.Log(" Here are the recovered cases " + totalData.recovered);
                //Debug.Log(" Here are the casualities " + totalData.deaths);

                //infectedText.text = totalData.confirmed.ToString();
                //recoveredText.text = totalData.recovered.ToString();
                //casualtyText.text = totalData.deaths.ToString();



                //DataTypeVisualizer dataType = JsonUtility.FromJson<DataTypeVisualizer>(jsonResult);
            }
        }
    }
}
