﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine.UI;


public class GlobalDataManager : MonoBehaviour
{
    public Text confirmed_value;
    public Text recovered_value;
    public Text tested_value;

    void Start()
    {
        StartCoroutine(getData());
    }

    IEnumerator getData()
    {
        UnityWebRequest www = UnityWebRequest.Get("https://corona.lmao.ninja/v2/all");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {


            byte[] results = www.downloadHandler.data;

            if (www.isDone)
            {
                string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                Debug.Log(jsonResult);
                

                GlobalDataController globalDataController = JsonUtility.FromJson<GlobalDataController>(jsonResult);

                Debug.Log(globalDataController.cases);
                Debug.Log(globalDataController.recovered);
                Debug.Log(globalDataController.tests);


                confirmed_value.text = globalDataController.cases.ToString("0,0,0,0");
                recovered_value.text = globalDataController.recovered.ToString("0,0,0,0");
                tested_value.text = globalDataController.tests.ToString("0,0,0,0");
                //Debug.Log(" Here are the confirmed cases " + totalData.confirmed);
                //Debug.Log(" Here are the recovered cases " + totalData.recovered);
                //Debug.Log(" Here are the casualities " + totalData.deaths);

                //infectedText.text = totalData.confirmed.ToString();
                //recoveredText.text = totalData.recovered.ToString();
                //casualtyText.text = totalData.deaths.ToString();



                //DataTypeVisualizer dataType = JsonUtility.FromJson<DataTypeVisualizer>(jsonResult);
            }
        }
    }
}
