﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

[Serializable]
public class LocationDataController
{

    public List<DataItem> data;
    
    public double dt;

    public double ts;
}

[Serializable]
public class DataItem
{
    public string Province_State;

    public int Confirmed;

    public int Deaths;

    public int Recovered;

    public int Active;

    public int People_Tested;

    public int People_Hospitalized;
}
