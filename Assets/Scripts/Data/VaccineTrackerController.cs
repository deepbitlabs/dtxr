﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaccineTrackerController
{

    public string source;

    public int totalCandidates;

    public List<PhasesItem> phases;

    public List<DataItemList> data;
}


public class PhasesItem
{
    public string phase;

    public int candidates;
}

public class DataItemList
{

    public string candidate;

    public List<string> sponsors;

    public string details;

    public string trialPhase;

    public List<string> institutions;

    public List<string> funding;
}
